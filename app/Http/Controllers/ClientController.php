<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $itemsPerPage = 10;

        $clients = Client::orderBy('id', 'ASC')->latest()->paginate($itemsPerPage);

        return view('clients.index', compact('clients'))->with('i', ((request()->input('page', 1) - 1) * $itemsPerPage));

    }//end index()


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clients.create');

    }//end create()


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(
            [
                'name' => 'required',
                'surname' => 'required',
            ]
        );
        Client::create($request->all());

        return redirect()->route('clients.index')->with('success', 'Client created successfully');

    }//end store()


    /**
     * Display the specified resource.
     *
     * @param integer $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::find($id);

        return view('clients.show', compact('client'));

    }//end show()


    /**
     * Show the form for editing the specified resource.
     *
     * @param integer $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::find($id);

        return view('clients.edit', compact('client'));

    }//end edit()


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param integer                  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate(
            [
                'name' => 'required',
                'surname' => 'required',
            ]
        );
        Client::find($id)->update($request->all());

        return redirect()->route('clients.index')->with('success', 'Client updated successfully');

    }//end update()


    /**
     * Remove the specified resource from storage.
     *
     * @param integer $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Client::find($id)->delete();

        return redirect()->route('clients.index')->with('success', 'Client deleted successfully');

    }//end destroy()


}//end class
