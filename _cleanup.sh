#!/bin/bash

file=$1
jbformatter=~/.local/share/JetBrains/Toolbox/apps/PhpStorm/ch-0/182.4892.16/bin/format.sh

if [[ -n "$file" ]]; then
    ~/_cleanup_mightymobile.sh "$file"
    $jbformatter -s ~/phpstorm_remotessh.xml "$file"
else
    find ./database/seeds/ -type f -iname "*Seeder.php" -exec ~/_cleanup_mightymobile.sh {} \;
    $jbformatter -s ~/phpstorm_remotessh.xml -m "*Seeder.php" -r ./database/seeds/

    find ./database/migrations/ -type f -iname "*.php" -exec ~/_cleanup_mightymobile.sh {} \;
    $jbformatter -s ~/phpstorm_remotessh.xml -m "*.php" -r ./database/migrations/

    find ./app/ -type f -iname "*.php" -exec ~/_cleanup_mightymobile.sh {} \;
    $jbformatter -s ~/phpstorm_remotessh.xml -r ./app/

    find ./routes/ -type f -iname "*.php" -exec ~/_cleanup_mightymobile.sh {} \;
    $jbformatter -s ~/phpstorm_remotessh.xml -r ./routes/

    find ./config/ -type f -iname "*.php" -exec ~/_cleanup_mightymobile.sh {} \;
    $jbformatter -s ~/phpstorm_remotessh.xml -r ./config/
fi

find . -name "*~" -exec rm {} \; -exec git rm {} \;
find . -name ".php_cs.cache" -exec rm {} \; -exec git rm {} \;
