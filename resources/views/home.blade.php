@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        Welcome to the Table Charm demo app.
                        <br><br>
                        Click <a href="{{ route('clients.index') }}"> here </a> to access the clients
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
