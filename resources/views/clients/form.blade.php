<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <table class="table table-bordered">
            <tbody>
            <tr>
                <td><strong>Name</strong></td>
                <td>{!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}</td>
            </tr>
            <tr>
                <td><strong>Surname</strong></td>
                <td>{!! Form::text('surname', null, array('placeholder' => 'Surname','class' => 'form-control')) !!}</td>
            </tr>
            <tr>
                <td><strong>ID Number</strong></td>
                <td>{!! Form::text('id_number', null, array('placeholder' => 'ID Number','class' => 'form-control')) !!}</td>
            </tr>
            <tr>
                <td><strong>Cellphone Number</strong></td>
                <td>{!! Form::text('cellphone', null, array('placeholder' => 'Cellphone Number','class' => 'form-control')) !!}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>