@extends('home')

@section('content')
    <div class="row">
        <div class="col-lg-10 margin-tb col-md-offset-1">
            <div class="pull-left">
                <h2>Show Client</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('clients.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td><strong>Name</strong></td>
                    <td>{{ $client->name}}</td>
                </tr>
                <tr>
                    <td><strong>Surname</strong></td>
                    <td>{{ $client->surname}}</td>
                </tr>
                <tr>
                    <td><strong>ID Number:</strong></td>
                    <td>{{ $client->id_number}}</td>
                </tr>
                <tr>
                    <td><strong>Cellphone Number:</strong></td>
                    <td>{{ $client->cellphone}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection