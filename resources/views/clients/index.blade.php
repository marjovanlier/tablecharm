@extends('home')


@section('content')
    <div class="row">
        <div class="col-lg-10 margin-tb col-md-offset-1">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('clients.create') }}"> Create New Client</a>
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <br>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <table class="table table-bordered">
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>ID Number</th>
                    <th>Cellphone Number</th>
                    <th width="280px">Action</th>
                </tr>
                @foreach ($clients as $client)
                    <tr>
                        <td>{{ $client->id }}</td>
                        <td>{{ $client->name}}</td>
                        <td>{{ $client->surname}}</td>
                        <td>{{ $client->id_number}}</td>
                        <td>{{ $client->cellphone}}</td>
                        <td>
                            <a class="btn btn-info" href="{{ route('clients.show',$client->id) }}">Show</a>
                            <a class="btn btn-primary" href="{{ route('clients.edit',$client->id) }}">Edit</a>


                            {!! Form::open(['method' => 'DELETE','route' => ['clients.destroy', $client->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            </table>
            <div >
                {!! $clients->links() !!}
            </div>
        </div>
    </div>

@endsection