<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{


    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create(
            'clients',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('surname');
                $table->integer('id_number');
                $table->integer('cellphone');
                $table->timestamps();
            }
        );

    }//end up()


    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('clients');

    }//end down()


}//end class
